package com.sda.model;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class EnglishSpeaker implements Speaker, InitializingBean, DisposableBean {
	
	private Writer writer;
	
	public EnglishSpeaker(Writer writer) {
		this.writer = writer;
	}

	public void sayHello() {
		writer.writeText("Hello");
	}

	public void destroy() throws Exception {
		System.out.println("EnglishSpeaker destroyed");
	}

	public void afterPropertiesSet() throws Exception {
		System.out.println("EnglishSpeaker initialized");
	}

}
