package com.sda.model;

import java.io.IOException;

public interface Writer {
	
	void writeText(String text);

}
