package com.sda.main;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sda.model.EnglishSpeaker;
import com.sda.model.Speaker;

public class App {

	public static void main(String[] args) {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("bean-configuration.xml");
		
		// Speaker englishSpeaker = context.getBean(EnglishSpeaker.class);
		Speaker englishSpeaker = context.getBean("englishSpeaker", Speaker.class);
		englishSpeaker.sayHello();

		Speaker frenchSpeaker = context.getBean("frenchSpeaker", Speaker.class);
		frenchSpeaker.sayHello();
		
		context.close();
	}

}
