package com.sda.model;

import java.util.List;

public class MultiWriter implements Writer {
	
	private List<Writer> writers;
	
	public MultiWriter(List<Writer> writers) {
		this.writers = writers;
	}

	@Override
	public void writeText(String text) {
		for (Writer w : writers) {
			w.writeText(text);
		}
	}

}
