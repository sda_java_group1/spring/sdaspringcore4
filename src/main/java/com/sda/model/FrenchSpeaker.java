package com.sda.model;

public class FrenchSpeaker implements Speaker {

	private Writer writer;
	
	public FrenchSpeaker(Writer writer) {
		this.writer = writer;
	}

	public void sayHello() {
		writer.writeText("Bonjour");
	}
	
	public void initialized() {
		System.out.println("FrenchSpeaker initialized");
	}

	public void destroyed() {
		System.out.println("FrenchSpeaker destroyed");
	}
}
