package com.sda.model;

import java.io.BufferedWriter;
import java.io.IOException;

public class FileWriter implements Writer {

	public void writeText(String text) {
		/* BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new java.io.FileWriter("log.txt", true));
			writer.write(text);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				writer.close();
			} catch (IOException e) { }
		} */
		
		try (BufferedWriter writer = new BufferedWriter(new java.io.FileWriter("log.txt", true))) {
			writer.write(text);
			writer.write('\n');
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

}
